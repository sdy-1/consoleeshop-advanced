﻿using System.Collections.Generic;
using OnlineStoreConsoleUI.Commands;

namespace OnlineStoreConsoleUI.Controllers
{
    public class GuestController : Controller
    {
        public override List<CommandBase> Commands { get; } = new List<CommandBase>
        {
            new FindProductCommand(),
            new HelpCommand(),
            new RegisterCommand(),
            new LoginCommand(),
            new ShowAllProductsCommand()
        };
    }
}