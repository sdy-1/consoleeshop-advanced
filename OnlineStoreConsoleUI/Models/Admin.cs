﻿using System.Collections.Generic;
using System.Linq;
using OnlineStoreDomain.Repositories;

namespace OnlineStoreConsoleUI.Models
{
    public class Admin : RegisteredUser
    {
        public virtual List<Customer> GetAllCustomers() =>
            CustomerRepository.Customers.ToList();
    }
}