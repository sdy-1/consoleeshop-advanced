﻿using System;
using OnlineStoreConsoleUI.Controllers;

namespace OnlineStoreConsoleUI.Commands
{
    public class ShowOrdersCommand : CommandBase
    {
        public override string Name => "show orders";
        public override string Description => "Show my orders";

        public override Controller Execute<T>(T controller)
        {
            if (!(controller is CustomerController customerController))
                return controller;

            foreach (var order in customerController.CurrentCustomer.PlacedOrders)
            {
                Console.WriteLine($"{order} - {order.Status}");
            }

            return customerController;
        }
    }
}