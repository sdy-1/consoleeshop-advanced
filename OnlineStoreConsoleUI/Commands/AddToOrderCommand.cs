﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Models;
using OnlineStoreConsoleUI.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class AddToOrderCommand : CommandBase
    {
        public override string Name => "add";
        public override string Description => "Add a product to order";

        public override Controller Execute<T>(T controller)
        {
            if (!(controller is CustomerController customerController))
                return controller;

            var products = ProductRepository.Products;
            
            while (true)
            {
                Console.WriteLine("Press y to create a new order.");
                Console.WriteLine("Press n to go back.");
                var key = Console.ReadKey();
                Console.WriteLine();

                switch (key.Key)
                {   
                    case ConsoleKey.Y:
                        if (AddToOrder(products, customerController))
                            return customerController;
                        break;
                    
                    case ConsoleKey.N:
                        return customerController;
                    
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong key");
                        Console.ResetColor();
                        break;
                }
            }
        }

        private static bool AddToOrder(IEnumerable<Product> products, CustomerController customerController)
        {
            Console.WriteLine("Enter a name of product");
            var name = Console.ReadLine();
            var product = products.FirstOrDefault(p => p.Name == name);

            if (product is null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Product was not found");
                Console.ResetColor();
                return false;
            }

            Console.WriteLine("Enter a quantity of product");
            int quantity;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out quantity))
                {
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("A string is not a number");
                Console.ResetColor();
            }

            customerController.CurrentCustomer.AddProductToOrder(product, quantity);
            Console.WriteLine("Product was successfully added");
            
            return true;
        }
    }
}