﻿using System.Collections.Generic;
using OnlineStoreConsoleUI.Models;

namespace OnlineStoreDomain.Repositories
{
    public static class CustomerRepository
    {
        private static List<Customer> _customers;

        public static IEnumerable<Customer> Customers => _customers;

        static CustomerRepository()
        {
            _customers = new List<Customer>();
            
            var array = new Customer[]
            {
                new Customer {Login = "customer1", Password = "pass1"},
                new Customer {Login = "customer2", Password = "pass2"},
                new Customer {Login = "customer3", Password = "pass3"}
            };
        }
        
        // private static void Create()
        // {
        //     var customers = new Customer[]
        //     {
        //         new Customer {Login = "customer1", Password = "pass1"},
        //         new Customer {Login = "customer2", Password = "pass2"},
        //         new Customer {Login = "customer3", Password = "pass3"}
        //     };
        //
        //     foreach (var customer in customers)
        //     {
        //         _customers.Add(customer);
        //     }
        // }

        public static void RegisterCustomer(Customer customer) =>
            _customers.Add(customer);
    }
}