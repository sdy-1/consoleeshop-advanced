﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Models;
using OnlineStoreDomain.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class ChangeUserInfoCommand : CommandBase
    {
        private static Dictionary<int, Action<CustomerController>> _actions = new Dictionary<int, Action<CustomerController>>();
        public override string Name => "change user info";
        public override string Description => "Change the info of a user";
        static ChangeUserInfoCommand()
        {
            _actions.Add(1, ChangeName);
            _actions.Add(2, ChangeEmail);
            _actions.Add(3, ChangePhoneNumber);
            _actions.Add(4, ChangeAddress);
        }
        public override Controller Execute<T>(T controller)
        {
            if (!(controller is AdminController adminController))
                return controller;

            while (true)
            {
                Console.WriteLine("1. Change customer's info");
                Console.WriteLine("0. Go back");

                int key;

                while (true)
                {
                    if (int.TryParse(Console.ReadLine(), out key) && key >= 0 && key <= 1)
                        break;

                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Wrong input");
                    Console.ResetColor();
                }

                switch (key)
                {
                    case 1:
                        var customer = GetUser();
                        var customerController = new CustomerController(customer);
                        ChangeInfo(customerController);
                        break;

                    case 0:
                        return adminController;
                }
            }
        }
        
        private static Customer GetUser()
        {
            while (true)
            {
                Console.WriteLine("Enter user's login");
                var login = Console.ReadLine();
                var customer = CustomerRepository.Customers.FirstOrDefault(c => c.Login == login);

                if (customer is { })
                    return customer;

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Customer with this login was not found");
                Console.ResetColor();
            }
        }
        
        private static void ChangeInfo(CustomerController customerController)
        {
            Console.WriteLine(customerController.CurrentCustomer);
            
            while (true)
            {
                Console.WriteLine("1. Change name"); 
                Console.WriteLine("2. Change email"); 
                Console.WriteLine("3. Change phone number"); 
                Console.WriteLine("4. Change address");

                int key;

                while (true)
                {
                    if (int.TryParse(Console.ReadLine(), out key) && key >= 1 && key <= 4) 
                        break;

                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Wrong input");
                    Console.ResetColor();
                }
                
                _actions[key].Invoke(customerController);
                break;
            }
        }

        private static void ChangeAddress(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var address = Console.ReadLine();

                if (address is { })
                {
                    customerController.CurrentCustomer.Address = address;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void ChangePhoneNumber(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a phone number");
                var number = Console.ReadLine();

                var pattern = @"/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/";
                if (number is { } && Regex.IsMatch(number, pattern))
                {
                    customerController.CurrentCustomer.PhoneNumber = number;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void ChangeEmail(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter an email");
                var email = Console.ReadLine();

                try
                {
                    if (email is { })
                        _ = new MailAddress(email);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Incorrect date of birth");
                    Console.ResetColor();
                }

                customerController.CurrentCustomer.Email = email;

                break;
            }
        }

        private static void ChangeName(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var name = Console.ReadLine();

                var pattern = @"[a-zA-Z]";
                if (name is { } && Regex.IsMatch(name, pattern))
                {
                    customerController.CurrentCustomer.Name = name;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }
    }
}