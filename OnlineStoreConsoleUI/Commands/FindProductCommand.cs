﻿using System;
using System.Linq;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class FindProductCommand : CommandBase
    {
        public override string Name => "find";
        public override string Description => "Find a product by name";

        public override Controller Execute<T>(T controller)
        {
            var products = ProductRepository.Products;

            while (true)
            {
                Console.WriteLine("Enter a product name");
                var name = Console.ReadLine();

                var product = products.FirstOrDefault(pr => pr.Name == name);

                if (product is { })
                {
                    Console.WriteLine(product);
                    return controller;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }
    }
}