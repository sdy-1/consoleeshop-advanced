﻿using System.Collections.Generic;
using OnlineStoreConsoleUI.Models;

namespace OnlineStoreConsoleUI.Repositories
{
    public static class AdminRepository
    {
        private static List<Admin> _admins = new List<Admin>();

        public static IEnumerable<Admin> Admins => _admins;

        static AdminRepository()
        {
            _admins.Add(new Admin { Login = "admin", Password = "pass" });
        }
    }
}