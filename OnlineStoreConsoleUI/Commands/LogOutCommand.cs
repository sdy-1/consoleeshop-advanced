﻿using System;
using OnlineStoreConsoleUI.Controllers;

namespace OnlineStoreConsoleUI.Commands
{
    public class LogOutCommand : CommandBase
    {
        public override string Name => "log out";
        public override string Description => "Log out from account";

        public override Controller Execute<T>(T controller)
        {
            Console.WriteLine("You have successfully logged out");
            return new GuestController();
        }
    }
}