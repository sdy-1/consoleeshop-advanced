﻿using System.Collections.Generic;
using OnlineStoreConsoleUI.Commands;

namespace OnlineStoreConsoleUI.Controllers
{
    public abstract class Controller
    {
        public abstract List<CommandBase> Commands { get; }
    }
}