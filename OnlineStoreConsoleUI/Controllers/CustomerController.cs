﻿using System;
using System.Collections.Generic;
using OnlineStoreConsoleUI.Commands;
using OnlineStoreConsoleUI.Models;

namespace OnlineStoreConsoleUI.Controllers
{
    public class CustomerController : Controller
    {
        public Customer CurrentCustomer { get; set; }
        
        public override List<CommandBase> Commands { get; } = new List<CommandBase>
        {
            new AddToOrderCommand(),
            new CancelOrderCommand(),
            new ChangePersonalInfoCommand(),
            new FindProductCommand(),
            new HelpCommand(),
            new LoginCommand(),
            new LogOutCommand(),
            new OrderCreateCommand(),
            new OrderReceivedCommand(),
            new ShowOrdersCommand()
        };

        public CustomerController(Customer customer)
        {
            _ = customer ?? throw new ArgumentNullException(nameof(customer));

            CurrentCustomer = customer;
        }
    }
}