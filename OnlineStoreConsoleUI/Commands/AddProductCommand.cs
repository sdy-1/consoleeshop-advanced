﻿using System;
using System.Linq;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Models;
using OnlineStoreConsoleUI.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class AddProductCommand : CommandBase
    {
        public override string Name => "add product";
        public override string Description => "Add a new product to the store";

        public override Controller Execute<T>(T controller)
        {
            if (!(controller is AdminController adminController))
                return controller;
            
            var products = ProductRepository.Products;

            string name;
            while (true)
            {
                Console.WriteLine("Enter a name of product");
                name = Console.ReadLine();

                if (name is { } && !products.Any(p => p.Name == name))
                    break;
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The product with this name is already exists");
                Console.ResetColor();
            }

            Console.WriteLine("Choose category of a product");
            foreach (var categoryName in Enum.GetNames(typeof(Category)))
            {
                Console.WriteLine(categoryName);
            }

            Category category;
            while (true)
            {
                if (Enum.TryParse(Console.ReadLine(), out category))
                    break;
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("This category does not exist");
                Console.ResetColor();
            }

            Console.WriteLine("Write a description");
            string description = Console.ReadLine();

            Console.WriteLine("Enter a price of product");
            decimal price;
            while (true)
            {
                if (decimal.TryParse(Console.ReadLine(), out price) && price > 0) 
                    break;
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong price input");
                Console.ResetColor();
            }
            
            var product = new Product(name, price, description, category);

            Console.WriteLine("You successfully added a new product");
            
            return adminController;
        }
    }
}