﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;
using OnlineStoreConsoleUI.Controllers;

namespace OnlineStoreConsoleUI.Commands
{
    public class ChangePersonalInfoCommand : CommandBase
    {
        private static readonly Dictionary<int, Action<CustomerController>> _actions = new Dictionary<int, Action<CustomerController>>();
        public override string Name => "change info";
        public override string Description => "Change personal information";

        static ChangePersonalInfoCommand()
        {
            _actions.Add(1, ChangeName);
            _actions.Add(2, ChangeEmail);
            _actions.Add(3, ChangePhoneNumber);
            _actions.Add(4, ChangeAddress);
        }
        public override Controller Execute<T>(T controller)
        {
            if (!(controller is CustomerController customerController))
                return controller;
            
            
            Console.WriteLine(customerController.CurrentCustomer);
            
            while (true)
            {
                Console.WriteLine("1. Change name"); 
                Console.WriteLine("2. Change email"); 
                Console.WriteLine("3. Change phone number"); 
                Console.WriteLine("4. Change address");
                Console.WriteLine("0. Go back");

                int key;

                while (true)
                {
                    if (int.TryParse(Console.ReadLine(), out key) && key >= 0 && key <= 4) 
                        break;

                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Wrong input");
                    Console.ResetColor();
                }

                if (key == 0) return customerController;
                _actions[key].Invoke(customerController);
            }
        }

        private static void ChangeAddress(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var address = Console.ReadLine();

                if (address is { })
                {
                    customerController.CurrentCustomer.Address = address;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void ChangePhoneNumber(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a phone number");
                var number = Console.ReadLine();

                var pattern = @"/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/";
                if (number is { } && Regex.IsMatch(number, pattern))
                {
                    customerController.CurrentCustomer.PhoneNumber = number;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void ChangeEmail(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter an email");
                var email = Console.ReadLine();

                try
                {
                    if (email is { })
                        _ = new MailAddress(email);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Incorrect date of birth");
                    Console.ResetColor();
                }

                customerController.CurrentCustomer.Email = email;

                break;
            }
        }

        private static void ChangeName(CustomerController customerController)
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var name = Console.ReadLine();

                var pattern = @"[a-zA-Z]";
                if (name is { } && Regex.IsMatch(name, pattern))
                {
                    customerController.CurrentCustomer.Name = name;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }
    }
}