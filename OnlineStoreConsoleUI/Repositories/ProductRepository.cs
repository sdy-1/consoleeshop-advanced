﻿using System.Collections.Generic;
using OnlineStoreConsoleUI.Models;

namespace OnlineStoreConsoleUI.Repositories
{
    public static class ProductRepository
    {
        private static List<Product> _products = new List<Product>();

        public static IEnumerable<Product> Products => _products;

        static ProductRepository() => Create();

        public static void AddProduct(Product product)
        {
            _products.Add(product);
        }
        
        private static void Create()
        {
            var products = new Product[]
            {
                new Product("Apple", 10, "Fruit", Category.Fruit),
                new Product("Juice", 12, "Some tasty thing", Category.Drink),
                new Product("Carrot", 4, "Vegetable", Category.Vegetable),
                new Product("Chicken", 30, "From country", Category.Meat),
                new Product("Fish", 43, "With love from Black Sea", Category.Meat)
            };
        }
    }
}

